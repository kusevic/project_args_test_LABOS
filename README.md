# Simple File Sharing Server (simplefss)

SimpleFSS je jednostavan HTTP server koji se pokrene u nekom određenom
direktoriju pozivanjem naredbe `simplefss putanja/do/direktorija` ili 
`simplefss .` za pokretanje aplikacije u trenutnom direktoriju.

SimpleFSS omogućuje upload i download podataka preko lokalne mreže u ili iz 
direktorija u kojem je pokrenut. Server će se pokrenuti na
localhostu na određenom portu (defaultni port je `8080`) i omogućiti upload ili
download datoteka. 

### Pristup

- Za pristup preko računala s kojeg je pokrenut dovoljno je upisati u browser
  `localhost:port`, gdje je `port` jednak broju porta na kojem je server
  pokrenut. Defaultni port je `8080`, pa je tako defaultna adresa 
  `localhost:8080`.
- Za pristup preko mreže upišite u browser `ip_adresa_racunala:port`, gdje je
  `ip_adresa_racunala` jednaka IP adresi računala na kojem je server pokrenut,
  dok je `port` jednak broju porta na kojem je server pokrenut.

## Instalacija 

Klonirajte repozitorij na svoje računalo.

## Development

Za lokalnu instalaciju radi razvoja SimpleFSS-a preporučujemo lokalnu
instalaciju pomoću PIP-a:

```
sudo pip install -e /putanja/gdje/je/projekt/kloniran
```

ili ukoliko se nalazite u direktoriju gdje projekt kloniran:
    
```
sudo pip install -e .
```

Ova naredba će instalirati vaš projekt kao python modul, čime ćemo ga moći
importati u pytest testove. Pri ovakvoj instalaciji svi fileovi s programskim
kodom  ostaju u direktoriju gdje je kloniran te se pri svakoj promjeni koda
pokreće zadnja verzija koda. 

Za pokretanje programa jednostavno pokrenite:

```
simplefss /putanja/do/direktorija
```



