import os, sys
import pkg_resources
import utils
import settings
import logging

class ErrorLogger():

    def log(self, path, message, error_code):
        logger = logging.getLogger('myapp')
        hdlr = logging.FileHandler('./errors.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr) 
        logger.setLevel(logging.WARNING)
        logger.error('Putanja je: ' + ' ' + path + ' ,odgovor servera: ' + ' ' + message + ' kod greske je: ' + str(error_code))      

class CaseError(Exception):
    """Exception raised for incorrect case handling

    Attributes: 
        path -- input path that caused the exception
        message -- explanation of the error
        error_code -- error code to return
    """

    def __init__(self, path, message, error_code):
        self.path = path
        self.message = message
        self.error_code = error_code
        logger = ErrorLogger()
        logger.log(path, message, error_code)


class CaseSiteRootHandler:
    """Handles the case when the root url for app is requested: 
    http://localhost:port. Returns the index.html page from www
    directory"""
    def test(self, file_path):
        if file_path == "/":
            return True
        else:
            return False
    def run(self, file_path):
        index = utils.get_asset_full_path("/www/index.html")
        print "Showing: ", index
        try: 
            with open(index, "rb") as reader:
                content = reader.read()
            return content
        except Exception:
            raise CaseError(file_path, "Internal Server Error", 500)
        


        
class CaseFileExistsHandler:
    def test(self, file_path):
        if os.path.isfile(file_path):
            return True
        else: 
            return False

    def run(self, file_path):
        try: 
            with open(file_path, "rb") as reader:
                content = reader.read()
            return content
        except Exception:
            raise CaseError(file_path, "Internal server Error", 500)


class CaseFileNotExistsHandler:
    def test(self, file_path):
        if not os.path.isfile(file_path) and not os.path.isdir(file_path):
            return True
        else: 
            return False

    def run(self, file_path):
        if self.test(file_path):
            raise CaseError(file_path, "File not found", 404)
        else:
            raise CaseError(file_path, "Internal server Error", 500)



