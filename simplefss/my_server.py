import BaseHTTPServer
from case_handlers import *
import os
import sys
import settings
# TODO:
# - handle errors 
#   - 404 for file not found
#   - 501 for features not yet implemented
#   - 500 for all other errors
# - handle multiple files
# - handle different mime types
# - move handlers to other class
# - create simple config file
# - error log i access log
readOnly = False
class MySimpleServer(BaseHTTPServer.BaseHTTPRequestHandler):
    CASES = [
            CaseSiteRootHandler(),
            CaseFileExistsHandler(), 
            CaseFileNotExistsHandler(),
            ]
    
    # Obradi GET request
    def do_GET(self):
        print "Headers: " + str(self.headers)
        print "Path: ", self.path
        sys.stdout.flush()
        #os.getcwd -> get_current_working_directory
        #self.path -> putanja do filea kojeg browser trazi
        #print os.getcwd() + self.path
        try:
            file_path = self.path
            for case in self.CASES:
                if case.test(file_path):
                    self.send_content(case.run(file_path))
                    break
        except CaseError as e:
            self.handle_error(e.error_code, e.message)

    def full_file_path(self):
        return os.path.join(os.getcwd(), "www", self.path[1:])


    def handle_error(self, code, msg):
        self.send_response(code)
        self.send_header("Content-Type", "text/html")
        page = "<html><body><p>"+str(msg)+"</p></body></html>"
        self.send_header("Content-Lenght", str(len(page)))
        self.end_headers()
        self.wfile.write(page)


    def send_content(self, content):
        if(readOnly):
            print "Server is in Read-Only mode, cannot send files"
            return
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Lenght", str(len(content)))
        self.send_header("Cache-Control", "no-cache")
        self.end_headers()
        self.wfile.write(content)


def main():
    readOnly = False
    serverPort = 8080

    print "Starting web server..."

    # get full path to shared directory
    if len(sys.argv) < 2:
        print "Add path to shared directory as argument"
        sys.exit(1)
    else:
        share_root = utils.get_share_full_path(sys.argv[1])
    
    [serverPort, readOnly] = utils.parse_args(sys.argv)

    ##print len(sys.argv), readOnly, serverPort

    # Initialize settings globals with full path to share root
    settings.init(share_root)
    
    settings.options['serverPort'] = serverPort
    settings.options['readOnly'] = readOnly

    for i in settings.options.keys():
        print str(i) + ': ' + str(settings.options[i])

    serverAddress = ('', serverPort)
    server = BaseHTTPServer.HTTPServer(
                                serverAddress, MySimpleServer)
    server.serve_forever()


if __name__ == '__main__':
    main()



