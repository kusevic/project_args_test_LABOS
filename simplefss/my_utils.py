import os, sys
import settings
import pkg_resources

def get_asset_full_path(asset_path):
    """ 
    This function returns the full path for assets included in the project. 
    These assets are for example: index html page and various error html pages,
    as well as icons and similar assets. The requested asset will probably be something
    inside the www/ folder of the package, or a similar asset.

    asset_path - path to a resource for which you want full path on 
                 the filesystem. Probably something in www folder
    Returns full asset path on the filesystem """
    resource_package = __name__
    full_path = pkg_resources.resource_filename(resource_package, asset_path)
    return full_path


def get_resource_full_path(resource_path):
    """
    This function returns the full path on filesystem for a specified resource. For example:
    when user request a file that is shared through the simplefss app, this function will return
    full path on the filesystem to that file.
    The path to the file is resolved using the share_root global variable defined in 
    settings module and initialized in main function of server.py file.

    resource_path - path to resource requested
    Returns full path on the filesystem """

    share_root = settings.share_root
    if resource_path[0] == "/":
        shift = 1
    else: 
        shift = 0
    # Shift resource_path by one char if it contains leading slash
    full_path = os.path.join(share_root, resource_path[shift:])
    return full_path


def get_share_full_path(path):
    """ 
    This function returns the full path on filesystem to the root directory of 
    requested share.

    current_dir - current directory in terminal where the script is run
    path - path passed to the script in parameters
    Returns full path to root of shared directory """
    # check if absolute path
    if path[0] == "/":
        return path
    return os.path.realpath(os.path.join(os.getcwd(), path))

def parse_args(call_string):
    serverPort = 8080
    readOnly = False
    for i in range(2, len(call_string)):
        if call_string[i] == "--port" and i < len(call_string) - 1 and call_string[i + 1].isdigit():
            serverPort = int(call_string[i + 1])
        if call_string[i] == "--readonly":
            readOnly = True
    return (serverPort, readOnly)
