import pytest
import inspect, os, sys
from simplefss import utils
from simplefss import settings

@pytest.fixture()
def get_mock_cwd():
    """ Return mocked current directory for tests """
    return os.path.dirname(inspect.getfile(inspect.currentframe()))

@pytest.fixture()
def get_mock_abs(pytestconfig):
    """ Return root of the project as absolute path """
    return str(pytestconfig.rootdir) 

@pytest.fixture()
def set_cwd():
    """ Set current directory for each test """
    os.chdir(get_mock_cwd())

    yield

    

def test_share_absolute_path(set_cwd, get_mock_abs):
    absolute_path = get_mock_abs
    return_path = utils.get_share_full_path(absolute_path)
    assert absolute_path == return_path

def test_share_simple_relative_path(set_cwd):
    relative_path = "tests"
    valid_path = get_mock_cwd() + "/tests"
    return_path = utils.get_share_full_path(relative_path)
    assert valid_path == return_path

def test_share_current_dir_relative_path(set_cwd):
    relative_path = "."
    valid_path = get_mock_cwd() 
    return_path = utils.get_share_full_path(relative_path)
    assert valid_path == return_path

def test_share_long_relative_path(set_cwd):
    relative_path = "tests/test_assets/utils/utils2"
    valid_path = get_mock_cwd() + "/" + relative_path
    return_path = utils.get_share_full_path(relative_path)
    assert valid_path == return_path

def test_share_backwards_relative_path(set_cwd):
    relative_path = ".."
    last_slash = get_mock_cwd().rfind("/")
    valid_path = get_mock_cwd()[:last_slash] 
    return_path = utils.get_share_full_path(relative_path)
    assert valid_path == return_path

def test_share_complicated_backwards_relative_path(set_cwd):
    relative_path = "../tests/test_assets/utils/utils2"
    last_slash = get_mock_cwd().rfind("/")
    valid_path = get_mock_cwd()[:last_slash] + relative_path[2:]
    return_path = utils.get_share_full_path(relative_path)
    assert valid_path == return_path

def test_asset_full_path(get_mock_abs):
    """ Returns correct path to an asset """
    valid_path = get_mock_abs + "/simplefss/www/index.html"
    return_path = utils.get_asset_full_path("www/index.html")
    assert valid_path == return_path

def test_resource_full_path_given_relative_path(set_cwd, get_mock_abs):
    """ Return correct path to a resource passed as parameter """
    # GIVEN the share root set to project_root/tests and relative path to resource
    # WHEN a path is set to test_assets/file.html
    # THEN the path to resource is project_root/tests/test_assets/file.html

    share_root = get_mock_abs + "/tests"
    settings.share_root = share_root
    valid_path = share_root + "/test_assets/file.html"
    return_path = utils.get_resource_full_path("test_assets/file.html")
    assert valid_path == return_path

def test_resource_full_path_given_absolute_path(set_cwd, get_mock_abs):
    """ Return correct path to a resource passed as parameter """
    # GIVEN the share root set to project_root/tests and absolute path to resource
    # WHEN a path is set to test_assets/file.html
    # THEN the path to resource is project_root/tests/test_assets/file.html

    share_root = get_mock_abs + "/tests"
    settings.share_root = share_root
    valid_path = share_root + "/test_assets/file.html"
    return_path = utils.get_resource_full_path("/test_assets/file.html")
    assert valid_path == return_path

def test_asset_return_content_given_absolute_path(get_mock_abs):
    asset_full_path = get_mock_abs + "/simplefss/www/error404.html"
    try:
        with open(asset_full_path, "rb") as reader:
            asset_full_path_content = reader.read()
    except IOError:
        print 'could not open file', asset_full_path

    content = utils.get_asset_content(404, "www")

    assert content == asset_full_path_content


