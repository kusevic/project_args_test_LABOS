import pytest
import inspect, os, sys
from simplefss import utils
from simplefss import settings

def test_first():
	arg = "simplefss . --readonly".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_second():
	arg = "simplefss . --readonly --port".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_third():
	arg = "simplefss . --readonly --port 99".split(' ')
	assert utils.parse_args(arg) == (99, True)

def test_fourth():
	arg = "simplefss .".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_fifth():
	arg = "simplefss . --port".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_sixth():
	arg = "simplefss . --port 9999".split(' ')
	assert utils.parse_args(arg) == (9999, False)